export default async function createList(boardId, listName) {
    try {
        const response = await fetch(`https://api.trello.com/1/lists?key=ffbf49859c4893fd9173220097e64330&token=f99f5b483f86659f167ddb52a4621e7608b7122ac1ecb9da02cafe4b94cb541f&name=${listName}&idBoard=${boardId}`, {
            method: 'POST'
        })
        console.log(response.status, response.statusText);
        return response.json();
    } catch (err) {
        console.error(err);
    }
}