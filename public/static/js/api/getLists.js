export default async function getLists(boardId){
  try{
    const response = await fetch(`https://api.trello.com/1/boards/${boardId}/lists?key=ffbf49859c4893fd9173220097e64330&token=f99f5b483f86659f167ddb52a4621e7608b7122ac1ecb9da02cafe4b94cb541f`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json'
      }
    })
    console.log(response.status, response.statusText);
    return response.json();
  } catch(err){
    console.error(err)
  }
}