import getLists from '../api/getLists.js';
import CardsView from "./Cards.js";
import boardName from '../api/getBoardDetail.js';
import createList from '../api/createList.js';

export default async function ListsView(boardId) {
    const listPage = document.createElement('ul');
    // listPage.innerHTML = '';
    const board = await boardName(boardId);
    console.log(board);
    listPage.innerHTML = `<h1 style="text-align: center;" class="font-bold text-3xl" >${board.name}</h1 style="text-align: center;">`
    listPage.className = 'font-bold whitespace-nowrap overflow-x-auto overflow-y-hidden';
    const lists = await getLists(boardId);
    console.log(lists);
    for(let i=0; i< lists.length; i++){
        const listItem = document.createElement('li');
        listItem.className = 'float-left m-4 list-item rounded p-4 font-bold text-xl'
        listItem.innerHTML = `<h4>${lists[i].name}</h4>`;

        const cardElement = await CardsView(lists[i].id);

        listItem.appendChild(cardElement);

        listPage.appendChild(listItem);
    }
    const addNewList = document.createElement("li");
    addNewList.className = 'add-new-list inline-block m-4 list-item rounded p-4 cursor-pointer';
    addNewList.innerText = '+ Add another list';
    addNewList.addEventListener('click', e=>addList(boardId))
    listPage.appendChild(addNewList)
    return listPage;
}


function addList(boardId) {
    const modalArea = document.querySelector('#modal-area');
    modalArea.classList.remove('hidden');
    while (modalArea.firstChild) {
        modalArea.removeChild(modalArea.lastChild);
    }
    modalArea.innerHTML = `
        <div id="create-list" class="modal-item p-2 rounded">
            <form>
                <div id="input-list" class="input-area">
                    <input type="text" placeholder="Add list title">
                </div>
                <div id="create-list-btn" class="form-btn float-left p-2 mt-3 rounded shadow cursor-pointer">
                    <input type="submit" value="submit">
                </div>
                <div id="cancel-list-btn" class="form-btn float-right p-2 mt-3 rounded shadow cursor-pointer">Cancel</div>
            </form>
        </div>`
        modalArea.firstElementChild.firstElementChild.lastElementChild.addEventListener('click', e=>{
            e.preventDefault();
            console.log(e.target);
            modalArea.classList.add('hidden');
        })
        modalArea.firstElementChild.firstElementChild.addEventListener('submit', async e=>{
            e.preventDefault();
            const listName = e.target.elements[0].value;
            console.log(listName==='')
            if(listName!==''){
                await createList(boardId, listName);
                location.reload();
            }
            modalArea.classList.add('hidden');
        })
}