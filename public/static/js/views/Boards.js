import getBoards from '../api/getBoards.js'
import createBoard from '../api/createBoard.js';

export default async function BoardView() {
    const boardContainer = document.createElement('div');
    boardContainer.id = 'board-container';
    const boardList = document.createElement('ul');
    let boardItems = ``;
    const boards = await getBoards();
    boards.forEach(board => {
        boardItems += `
        <li class="m-4 board-li float-left">
            <a href="/board/id:${board.id}" data-link>
                <div class='board-item pl-4 pt-2 font-bold'>${board.name}</div>
            </a>
        </li>`
    });
    boardList.innerHTML = boardItems;
    boardList.className = 'list-none p-8'
    const addNewBoard = document.createElement('li');
    addNewBoard.innerHTML = `<div class='w-full h-full cursor-pointer'>Create new board</div>`
    addNewBoard.className = 'm-4 add-board float-left';
    addNewBoard.addEventListener('click', addBoard);
    boardList.appendChild(addNewBoard);
    boardContainer.appendChild(boardList);
    return boardContainer;
}

function addBoard(){
    const modalArea = document.querySelector('#modal-area');
    modalArea.classList.remove('hidden');
    while (modalArea.firstChild) {
        modalArea.removeChild(modalArea.lastChild);
    }
    modalArea.innerHTML = `
        <div id="create-board" class="modal-item p-2 rounded">
            <form>
                <div id="input-board" class="input-area">
                    <input type="text" placeholder="Add board title">
                </div>
                <div id="create-board-btn" class="form-btn float-left p-2 mt-3 rounded shadow cursor-pointer">
                    <button type="submit" >Submit</button>
                </div>
                <div id="cancel-board-btn" class="form-btn float-right p-2 mt-3 rounded cursor-pointer shadow">Cancel</div>
            </form>
        </div>`
    modalArea.firstElementChild.firstElementChild.lastElementChild.addEventListener('click', e=>{
        e.preventDefault();
        console.log(e.target);
        modalArea.classList.add('hidden');
    })
    modalArea.firstElementChild.firstElementChild.addEventListener('submit', async e=>{
        e.preventDefault();
        const boardName = e.target.elements[0].value;
        console.log(boardName==='')
        if(boardName!==''){
            await createBoard(boardName);
            location.reload();
        }
        modalArea.classList.add('hidden');
    })
}
