import getChecklist from '../api/getChecklist.js';

export default async function Checklists(checklistId){
    const checklist = await getChecklist(checklistId);
    const checkListContainer = document.createElement('div');
    checkListContainer.innerHTML = `
        <div class="font-bold text-xl"><h2>${checklist.name}</h2></div>
    `
    const checkItemsList = checklist.checkItems;
    const checkItems = document.createElement('ul');
    checkItems.id = `${checklistId}`;
    checkItems.innerHTML = ``
    for(let i=0; i<checkItemsList.length; i++){
        const checkItem = checkItemsList[i];
        checkItems.innerHTML += `
            <li id="${checkItem.id}">
                <input type="checkbox">${checkItem.name}</input>
            </li>
        `
    }
    const checkListElement = document.createElement('div');
    checkListElement.appendChild(checkItems);
    checkListContainer.appendChild(checkListElement);
    return checkListContainer;
}