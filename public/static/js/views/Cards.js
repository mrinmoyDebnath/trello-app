import createCard from '../api/createCard.js';
import getAllCards from '../api/getCards.js';
import getCard from '../api/displayCard.js';
import Checklists from './Checklists.js';

export default async function CardsView(listId) {
    const cardsView = document.createElement('ul');
    cardsView.innerHTML = '';
    const cards = await getAllCards(listId);
    console.log(cards);
    cards.forEach(card => {
        cardsView.innerHTML += `<li id="${card.id}" class="card-item font-bold bg-white border-b-2 rounded shadow p-2 mt-2 cursor-pointer">${card.name}</li>`
    });
    const addNewCard = document.createElement('li');
    addNewCard.className = `font-bold add-new-card p-2 mt-2 cursor-pointer`
    addNewCard.innerText = '+ Add another card';
    addNewCard.addEventListener('click', e=>addCard(listId))
    cardsView.appendChild(addNewCard);
    cardsView.addEventListener('click', e=>displayCard(e))
    return cardsView;
}
async function displayCard(event){
    if(event.target.classList.contains('card-item')){
      const cardDetails = await getCard(event.target.id);
      console.log(cardDetails)
      cardPopUp(cardDetails)
    }
}
async function cardPopUp(cardDetails){
    const modalArea = document.querySelector('#modal-area');
    modalArea.classList.remove('hidden');
    while (modalArea.firstChild) {
        modalArea.removeChild(modalArea.lastChild);
    }
    modalArea.innerHTML = `
    <div id="card-popup" class="p-2 rounded text-left">
        <div class="card-heading text-2xl font-bold relative">
            <h1>${cardDetails.name}</h1>
            <div class="absolute right-0 -top-2 cursor-pointer">x</div>
        </div>
        <div class="card-content-container flex justify-between">
            <div class="card-content mt-2">
            </div>
            <div class="card-options"></div>
        </div>
    </div>`
    modalArea.firstElementChild.firstElementChild.lastElementChild.addEventListener('click', e=>{
        modalArea.classList.add('hidden');
    })
    const contentArea = modalArea.firstElementChild.lastElementChild.firstElementChild;
    if(cardDetails.desc!==''){
        contentArea.innerHTML = `
            <div class="my-2">
                <h2 class="font-bold text-xl">Description</h2>
                <p>${cardDetails.desc}</p>
            </div>
       `
    }
    if(cardDetails.idChecklists.length>0){
        for(let i=0; i<cardDetails.idChecklists.length; i++){
            const checklist = await Checklists(cardDetails.idChecklists[i])
            contentArea.appendChild(checklist);
        }
        console.log(contentArea)
    }
}

function addCard(listId){
    const modalArea = document.querySelector('#modal-area');
    modalArea.classList.remove('hidden');
    while (modalArea.firstChild) {
        modalArea.removeChild(modalArea.lastChild);
    }
    modalArea.innerHTML = `
    <div id="create-card" class="modal-item p-2 rounded">
        <form>
            <div id="input-card" class="input-area">
                <input type="text" placeholder="Add card title">
            </div>
            <div id="create-card-btn" class="form-btn float-left p-2 mt-3 rounded shadow cursor-pointer">
                <input type="submit" value="submit">
            </div>
            <div id="cancel-card-btn" class="form-btn float-right p-2 mt-3 rounded shadow cursor-pointer">Cancel</div>
        </form>
    </div>`
    modalArea.firstElementChild.firstElementChild.lastElementChild.addEventListener('click', e=>{
        e.preventDefault();
        console.log(e.target);
        modalArea.classList.add('hidden');
    })
    modalArea.firstElementChild.firstElementChild.addEventListener('submit', async e=>{
        e.preventDefault();
        const cardName = e.target.elements[0].value;
        console.log(cardName==='')
        if(cardName!==''){
            await createCard(listId, cardName);
            location.reload();
        }
        modalArea.classList.add('hidden');
    })
}
